import firebase from 'firebase';
import firestore from 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyDJnH1YVDgHrmxmqtLUSDmMFuukQmwYrSE',
  authDomain: 'realtime-chat-8acd0.firebaseapp.com',
  databaseURL: 'https://realtime-chat-8acd0.firebaseio.com',
  projectId: 'realtime-chat-8acd0',
  storageBucket: 'realtime-chat-8acd0.appspot.com',
  messagingSenderId: '223794913151',
};

const firebaseApp = firebase.initializeApp(config);
firebaseApp.firestore();

export default firebaseApp.firestore();
